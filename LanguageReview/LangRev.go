package main

import "fmt"

type person struct {
	fname string
	lname string
}

type secAgent struct {
	person
	licenceToKill bool
}

type human interface {
	speak()
}

func saySomething(h human) {
	h.speak()
}

func (p person) speak() {
	fmt.Println(p.fname, p.lname, "Hello")
}

func (s secAgent) speak() {
	fmt.Println(s.fname, s.lname, "Bye")
}

func main() {
	x := []int{2, 2, 4, 3, 3, 4}
	fmt.Println(x)

	m1 := map[string]int{
		"subash":     1,
		"pelaprlolu": 2,
	}
	fmt.Println(m1)

	for i, v := range m1 {
		fmt.Println("Name:", i, "Value:", v)
	}

	p1 := person{
		"subash",
		"Pelaprolu",
	}
	fmt.Println(p1)

	p1.speak()

	sa1 := secAgent{
		person{
			"subash11111",
			"Pelaprolu1111",
		},
		true,
	}

	fmt.Println(sa1)
	sa1.person.speak()

	saySomething(p1)
	saySomething(sa1)
}
