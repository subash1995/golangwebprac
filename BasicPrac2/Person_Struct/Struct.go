package main

import "fmt"

type person struct {
	fname string
	lname string
}

func main() {
	p1 := person{
		"Subash",
		"Pelaprolu",
	}

	fmt.Print(p1.fname)
}
