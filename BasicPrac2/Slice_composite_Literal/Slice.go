package main

import "fmt"

func main() {
	s := []int{1, 2, 3, 4, 5}
	fmt.Println(s)

	for k := range s {
		fmt.Println(k)
	}

	for k, v := range s {
		fmt.Println(k, v)
	}
}
