package main

import "fmt"

func main() {
	m := map[string]int{
		"subash":    1,
		"Pelaprolu": 2,
	}

	fmt.Println(m)

	for i := range m {
		fmt.Println(i)
	}

	for i, v := range m {
		fmt.Println(i, v)
	}
}
