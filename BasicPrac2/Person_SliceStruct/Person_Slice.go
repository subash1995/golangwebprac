package main

import "fmt"

type person struct {
	fname   string
	lname   string
	favfood []string
}

func (p person) walk() string {
	return fmt.Sprintln(p.fname, "is walking")
}

func main() {
	p1 := person{
		"Subash",
		"Pelaprolu",
		[]string{"ch", "lk"},
	}

	fmt.Println(p1)
	fmt.Println(p1.favfood)

	for i, v := range p1.favfood {
		fmt.Println(i, v)
	}

	s := p1.walk()

	fmt.Println(s)
}
