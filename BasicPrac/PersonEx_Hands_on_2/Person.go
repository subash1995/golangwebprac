package main

import "fmt"

type person struct {
	Fname string
	Lname string
}

type SecreteAgent struct {
	person
	LicenceTokill bool
}

func (p person) pspeak() {
	fmt.Println("Hello", p.Fname)
}

func (s SecreteAgent) sspeak() {
	fmt.Println("Agent Go", s.Fname, s.LicenceTokill)
}

func main() {
	p := person{
		"Subash",
		"Pelaprolu",
	}

	s := SecreteAgent{
		person{
			"subash",
			"Pelaprolu",
		},
		true,
	}

	p.pspeak()
	s.sspeak()
	s.pspeak()
}
