package main

import "fmt"

type person struct {
	fname string
	lname string
}

func (p person) Speak() {
	fmt.Println(p.fname, p.lname)
}

func main() {
	p1 := person{
		"subash",
		"pelaprolu",
	}
	p2 := person{
		"Nina",
		"Valentina",
	}
	fmt.Println(p1, p2)

	p1.Speak()
	p2.Speak()
}
