package main

import "fmt"

func main() {
	x := []int{1, 2, 3, 4, 5}
	for i, v := range x {
		fmt.Println(i, v)
	}

	y := make([]int, 0, 10)
	y = append(y, 1020)
	for i, v := range y {
		fmt.Println(i, v)
	}

}
