package main

import "fmt"

type person struct {
	fname string
	lname string
}

func main() {
	a1 := person{
		"subash",
		"pelaprolu",
	}
	fmt.Println(a1)

	a2 := struct {
		fname string
		lanem string
	}{
		"subash",
		"pelaprolu",
	}
	fmt.Println(a2)
}
