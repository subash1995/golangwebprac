package main

import "fmt"

func main() {
	x := map[string]int{
		"subash":    1,
		"Pelaprolu": 2,
	}
	for k, v := range x {
		fmt.Println(k, "-", v)
	}
}
