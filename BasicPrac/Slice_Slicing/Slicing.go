package main

import "fmt"

func main() {
	x := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	fmt.Println("Length", len(x), "Capacity", cap(x))

	for i, _ := range x {

		fmt.Println(x[i])

	}
	fmt.Println(x[9:])
}
