package main

import "fmt"

type dog int

type person struct {
	fname string
	lanme string
}

func (e dog) barknum() {
	fmt.Println("dog Num", e)
}

func (p person) speak() {
	fmt.Println("Person's first_name:", p.fname)
}

func main() {
	var d dog
	d = 1
	fmt.Printf("%T\n", d)
	fmt.Println(d)

	p1 := person{
		"subash",
		"Pelaprolu",
	}

	p2 := person{
		"Jaswnath",
		"Pelaprolu",
	}
	p1.speak()
	p2.speak()
}
