package main

import "fmt"

type person struct {
	fname string
	lname string
	age   int
}

type sa struct {
	person
	LicenceToKill bool
}

func (p person) speak() string {
	return fmt.Sprintln("FirstName", p.fname)
}

func (s sa) speak() string {
	return fmt.Sprintln("Secrete Agent", s.LicenceToKill)
}

type humanoid interface {
	speak() string
}

func vomit(h humanoid) {
	fmt.Println(h)
	switch v := h.(type) {
	case person:
		fmt.Println(v.fname)
	case sa:
		fmt.Println(v.LicenceToKill)
	default:
		fmt.Println("Unknown")

	}
}

func main() {
	p := person{
		"subash",
		"Pelaprolu",
		1,
	}
	s := sa{
		person{
			"Hello",
			"Bye",
			2,
		},
		false,
	}
	fmt.Printf("%T\n", s)
	vomit(p)
	vomit(s)
}
