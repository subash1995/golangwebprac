package main

import "fmt"

func main() {
	x := []int{1, 2, 3}
	fmt.Println(x)

	y := make([]int, 0, 100)
	y = append(y, 1)
	y = append(y, 2)
	y = append(y, 3)
	y = append(y, 4)
	fmt.Println(y, len(y), cap(y))
}
