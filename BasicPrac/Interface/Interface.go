package main

import (
	"fmt"
	"math"
)

type square struct {
	side float64
}

func (s square) area() float64 {
	return s.side * s.side
}

type circle struct {
	radius float64
}

func (c circle) area() float64 {
	return c.radius * c.radius * math.Pi
}

type shape interface {
	area() float64
}

func shapeArea(s shape) {
	fmt.Println("area is : ", s.area())

}

func main() {
	s := square{
		2,
	}

	c := circle{
		2,
	}

	shapeArea(s)
	shapeArea(c)
}
