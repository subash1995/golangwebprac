package main

import "fmt"

func main() {
	x := map[string]int{
		"subash":    1,
		"Pelaprolu": 2,
	}

	for k, _ := range x {
		fmt.Println(k, "-", x[k])
	}
}
