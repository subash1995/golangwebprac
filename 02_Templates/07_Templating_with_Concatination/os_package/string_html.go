package main

import (
	"fmt"
	"io"
	"log"
	"os"
	"strings"
)

func main() {
	name := "Subash Pelaprolu"

	str := fmt.Sprint(`
		
		<!DOCTYPE html>
		<html lang="en">
		<head>
		<meta charset="UTF-8">
		<title>Hello World</title>
		</head>
		<body>
		<>` + name + `</h1>
		</body>
		</html>
		
	`)

	nf, err := os.Create("index1.html")
	if err != nil {
		log.Fatal("error creating file", err)
	}
	defer nf.Close()

	io.Copy(nf, strings.NewReader(str))
}
