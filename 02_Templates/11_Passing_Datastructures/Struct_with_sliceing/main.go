package main

import (
	"html/template"
	"log"
	"os"
)

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseFiles("tpl.gohtml"))
}

type name struct {
	First string
	Last  string
}

func main() {

	n1 := name{
		First: "subash",
		Last:  "pelaprolu",
	}
	n2 := name{
		First: "Gandhi",
		Last:  "Mahatma",
	}
	n3 := name{
		First: "jaswanth",
		Last:  "chowdary",
	}

	names := []name{n1, n2, n3}

	err := tpl.Execute(os.Stdout, names)
	if err != nil {
		log.Fatalln(err)
	}
}
