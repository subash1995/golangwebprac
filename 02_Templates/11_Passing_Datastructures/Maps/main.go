package main

import (
	"log"
	"os"
	"text/template"
)

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseFiles("tpl.gohtml"))
}

func main() {
	val := map[string]string{
		"subash":    "Pelaprolu",
		"pelaprolu": "subash",
	}

	err := tpl.Execute(os.Stdout, val)
	if err != nil {
		log.Fatalln(err)
	}
}
