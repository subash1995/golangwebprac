package main

import (
	"html/template"
	"log"
	"os"
)

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseFiles("tpl.gohtml"))
}

type Name struct {
	First string
	Last  string
}

type Age struct {
	Years int
}

type Combi struct {
	T1 []Name
	T2 []Age
}

func main() {
	n1 := Name{
		First: "subash",
		Last:  "Pelaprolu",
	}
	n2 := Name{
		First: "Jaswanth",
		Last:  "Pelaprolu",
	}

	a1 := Age{
		Years: 1,
	}

	a2 := Age{
		Years: 2,
	}

	Names := []Name{n1, n2}
	Ages := []Age{a1, a2}

	c := Combi{
		T1: Names,
		T2: Ages,
	}

	err := tpl.Execute(os.Stdout, c)
	if err != nil {
		log.Fatalln(err)
	}

}
