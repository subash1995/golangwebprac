package main

import (
	"html/template"
	"log"
	"os"
)

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseFiles("tpl.gohtml"))
}

type name struct {
	First string
	Last  string
}

func main() {
	nam := name{
		First: "pelaprolu",
		Last:  "Subash",
	}

	err := tpl.Execute(os.Stdout, nam)
	if err != nil {
		log.Fatalln(err)
	}
}
