package main

import (
	"log"
	"os"
	"text/template"
)

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseFiles("tpl.gohtml"))
}

func main() {
	val := map[int]string{
		1: "subash",
		2: "pelaprolu",
	}

	err := tpl.Execute(os.Stdout, val)
	if err != nil {
		log.Fatalln(err)
	}
}
