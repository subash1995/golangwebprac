package main

import (
	"html/template"
	"log"
	"os"
)

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseFiles("tpl.gohtml"))
}

func main() {
	s := []string{"zero", "one", "Two"}

	err := tpl.ExecuteTemplate(os.Stdout, "tpl.gohtml", s)
	if err != nil {
		log.Fatalln(err)
	}
}
