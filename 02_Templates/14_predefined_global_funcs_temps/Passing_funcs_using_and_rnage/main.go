package main

import (
	"html/template"
	"log"
	"os"
)

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseFiles("tpl.gohtml"))

}

type user struct {
	Name  string
	Motto string
	Year  int
}

func main() {
	n1 := user{
		Name:  "subash",
		Motto: "hello",
		Year:  1995,
	}

	n2 := user{
		Name:  "Peleprolu",
		Motto: "bye",
		Year:  1995,
	}

	names := []user{n1, n2}

	err := tpl.ExecuteTemplate(os.Stdout, "tpl.gohtml", names)
	if err != nil {
		log.Fatalln(err)
	}
}
